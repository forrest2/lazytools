package main

import (
	"bufio"
	"errors"
	"fmt"
	"os"
	"strconv"
)

func scanner(ask string) string {
	scanner := bufio.NewScanner(os.Stdin)
	fmt.Println(ask)
	var userInput string
	for scanner.Scan() {
		userInput = scanner.Text()
		break
	}
	return userInput
}

func intConverter(userInput string) int {
	number, err := strconv.Atoi(userInput)
	var e error
	if err != nil {
		e = errors.New("requires an int")
		fmt.Println(e)
		os.Exit(1)
	}
	return number
}

func sumThis(x int, y int) int {
	return x + y
}

func main() {
	fmt.Println("Hi,", scanner("What's your name?"))
	fmt.Println("Your birthday is", scanner("What's your birthday?"))
	firstNumber := intConverter(scanner("Enter your first number:"))
	secondNumber := intConverter(scanner("Enter your second number:"))
	fmt.Println("The sum of your numbers is", sumThis(firstNumber, secondNumber))
}
