package dirutils

import (
	"os"
)

// CheckDir determines if a given path exists.
func CheckDir(path string) bool {
	if _, err := os.Stat(path); err != nil {
		if os.IsNotExist(err) {
			// file does not exist
			return false
		} else {
			// other error
			return false
		}
	}
	return true
}
