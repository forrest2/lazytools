package main

import (
	"fmt"
	"log"
	"os"

	"github.com/urfave/cli"
	"gitlab.com/forrest2/lazytools/elvui/src/dirutils"
)

var app = cli.NewApp()

func info() {
	app.Name = "ElvUI updater app"
	app.Usage = "Check and update ElvUI"
	app.Version = "0.0.1"
}

func main() {
	err := app.Run(os.Args)
	if err != nil {
		log.Fatal(err)
	}
}

func checkWoW() {
	wowRetail := "/Applications/World of Warcraft/_retail_/"

	fmt.Println("Checking for WoW Retail...")
	if dirutils.CheckDir(wowRetail) == true {
		fmt.Println("Found!")
	} else {
		fmt.Println("Specify custom WoW retail path")
	}
}

/*
This should check for WoW Retail
Then for ElvUI
Create a folder
*/
